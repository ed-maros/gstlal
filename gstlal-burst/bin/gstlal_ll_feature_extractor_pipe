#!/usr/bin/env python
#
# Copyright (C) 2011-2018 Chad Hanna, Duncan Meacher, Patrick Godwin
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""
This program makes a dag to run a series of gstlal_feature_extractor jobs online
"""

__author__ = 'Duncan Meacher <duncan.meacher@ligo.org>, Patrick Godwin <patrick.godwin@ligo.org>'

# =============================
#
#           preamble
#
# =============================

import optparse
import os

from gstlal import aggregator
from gstlal import inspiral_pipe

from gstlal.fxtools import feature_extractor
from gstlal.fxtools import multichannel_datasource
from gstlal.fxtools import multirate_datasource
from gstlal.fxtools import utils

# =============================
#
#          functions
#
# =============================

def generate_options(options):
	"""
	Generates a list of command line options to pass into DAG nodes.
	"""
	# data source options
	if options.data_source == 'lvshm':
		data_source_options = {
			"data-source": options.data_source,
			"shared-memory-partition": options.shared_memory_partition,
			"shared-memory-assumed-duration": options.shared_memory_assumed_duration
		}
	elif options.data_source == 'framexmit':
		data_source_options = {"data-source": options.data_source}

	# waveform options
	waveform_options = {
		"waveform": options.waveform,
		"mismatch": options.mismatch,
		"qhigh": options.qhigh
	}

	# data transfer options
	if options.save_format == 'kafka':
		save_options = {
			"save-format": options.save_format,
			"data-transfer": options.data_transfer,
			"kafka-partition": options.kafka_partition,
			"kafka-topic": options.kafka_topic,
			"kafka-server": options.kafka_server
		}
	elif options.save_format == 'hdf5':
		save_options = {
			"save-format": options.save_format,
			"cadence": options.cadence,
			"persist-cadence": options.persist_cadence
		}
	else:
		raise NotImplementedError, 'not an available option for online jobs at this time'

	# program behavior options
	program_options = {}
	if options.disable_web_service:
		program_options.update({"disable-web-service": options.disable_web_service})
	if options.verbose:
		program_options.update({"verbose": options.verbose})

	# gobble options together
	out_options = {}
	out_options.update(data_source_options)
	out_options.update(waveform_options)
	out_options.update(save_options)
	out_options.update(program_options)

	return out_options

def feature_extractor_node_gen(feature_extractor_job, dag, parent_nodes, ifo, options, data_source_info):
	feature_extractor_nodes = {}

	# generate common command line options
	command_line_options = generate_options(options)

	# parallelize jobs by channel subsets
	for ii, channel_subset in enumerate(data_source_info.channel_subsets):

		if options.verbose:
			print("Creating node for channel subset %d"%ii)

		# creates a list of channel names with entries of the form --channel-name=IFO:CHANNEL_NAME:RATE
		channels = [''.join(["--channel-name=",':'.join([channel, str(int(data_source_info.channel_dict[channel]['fsamp']))])]) for channel in channel_subset]
		channels[0] = channels[0].split('=')[1] # this is done to peel off --channel-name option off first channel

		# create specific options for each channel subset
		subset_options = {
			"max-streams": options.max_streams,
			"job-id": str(ii + 1).zfill(4),
			"channel-name":' '.join(channels)
		}
		subset_options.update(command_line_options)

		feature_extractor_nodes[ii] = \
			inspiral_pipe.generic_node(feature_extractor_job, dag, parent_nodes = parent_nodes,
				opts = subset_options,
				output_files = {"out-path": os.path.join(options.out_path, "gstlal_feature_extractor")}
			)

	return feature_extractor_nodes

# =============================
#
#     command line parser
#
# =============================

def parse_command_line():
	parser = optparse.OptionParser(usage = '%prog [options]', description = __doc__)

	# generic data source and feature extraction options
	multichannel_datasource.append_options(parser)
	feature_extractor.append_options(parser)

	# Condor commands
	group = optparse.OptionGroup(parser, "Condor Options", "Adjust parameters used for HTCondor")
	parser.add_option("--condor-command", action = "append", default = [], metavar = "command=value", help = "set condor commands of the form command=value; can be given multiple times")
	parser.add_option("--request-cpu", default = "2", metavar = "integer", help = "set the requested node CPU count for feature extraction jobs, default = 2")
	parser.add_option("--request-memory", default = "8GB", metavar = "integer", help = "set the requested node memory for feature extraction jobs, default = 8GB")
	parser.add_option("--auxiliary-request-cpu", default = "2", metavar = "integer", help = "set the requested node CPU count for auxiliary processes, default = 2")
	parser.add_option("--auxiliary-request-memory", default = "2GB", metavar = "integer", help = "set the requested node memory for auxiliary processes, default = 2GB")
	parser.add_option_group(group)

	# Synchronizer/File Sink commands
	group = optparse.OptionGroup(parser, "Synchronizer/File Sink Options", "Adjust parameters used for synchronization and dumping of features to disk.")
	parser.add_option("--tag", metavar = "string", default = "test", help = "Sets the name of the tag used. Default = 'test'")
	parser.add_option("--processing-cadence", type = "float", default = 0.1, help = "Rate at which the streaming jobs acquire and processes data. Default = 0.1 seconds.")
	parser.add_option("--request-timeout", type = "float", default = 0.2, help = "Timeout for requesting messages from a topic. Default = 0.2 seconds.")
	parser.add_option("--latency-timeout", type = "float", default = 5, help = "Maximum time before incoming data is dropped for a given timestamp. Default = 5 seconds.")
	parser.add_option_group(group)

	options, filenames = parser.parse_args()

	return options, filenames

# =============================
#
#             main
#
# =============================

#
# parsing and setting up core structures
#

options, filenames = parse_command_line()

data_source_info = multichannel_datasource.DataSourceInfo(options)
ifo = data_source_info.instrument
channels = data_source_info.channel_dict.keys()

#
# create directories if needed
#

listdir = os.path.join(options.out_path, "gstlal_feature_extractor/channel_lists")
aggregator.makedir(listdir)
aggregator.makedir("logs")

#
# set up dag and job classes
#

dag = inspiral_pipe.DAG("feature_extractor_pipe")

# feature extractor job
condor_options = {"request_memory":options.request_memory, "request_cpus":options.request_cpu, "want_graceful_removal":"True", "kill_sig":"15"}
condor_commands = inspiral_pipe.condor_command_dict_from_opts(options.condor_command, condor_options)
feature_extractor_job = inspiral_pipe.generic_job("gstlal_feature_extractor", condor_commands = condor_commands)

# auxiliary jobs
if options.save_format == 'kafka':
	auxiliary_condor_options = {"request_memory":options.auxiliary_request_memory, "request_cpus":options.auxiliary_request_cpu, "want_graceful_removal":"True", "kill_sig":"15"}
	auxiliary_condor_commands = inspiral_pipe.condor_command_dict_from_opts(options.condor_command, auxiliary_condor_options)
	synchronizer_job = inspiral_pipe.generic_job("gstlal_feature_synchronizer", condor_commands = auxiliary_condor_commands)
	hdf5_sink_job = inspiral_pipe.generic_job("gstlal_feature_hdf5_sink", condor_commands = auxiliary_condor_commands)

	#
	# set up options for auxiliary jobs
	#
	common_options = {
		"verbose": options.verbose,
		"tag": options.tag,
		"processing-cadence": options.processing_cadence,
		"request-timeout": options.request_timeout,
		"kafka-server": options.kafka_server
	}

	synchronizer_options = {
		"latency-timeout": options.latency_timeout,
		"input-topic-basename": options.kafka_topic,
		"output-topic-basename": '_'.join(['synchronizer', options.tag])
	}

	hdf5_sink_options = {
		"instrument": ifo,
		"channel-list": options.channel_list,
		"write-cadence": options.cadence,
		"persist-cadence": options.persist_cadence,
		"input-topic-basename": '_'.join(['synchronizer', options.tag])
	}

	synchronizer_options.update(common_options)
	hdf5_sink_options.update(common_options)

#
# set up jobs
#

feature_extractor_nodes = feature_extractor_node_gen(feature_extractor_job, dag, [], ifo, options, data_source_info)

if options.save_format == 'kafka':
	synchronizer_options.update({"num-topics": len(feature_extractor_nodes)})
	synchronizer_node = inspiral_pipe.generic_node(synchronizer_job, dag, [], opts = synchronizer_options, output_files = {"rootdir": os.path.join(options.out_path, "gstlal_feature_synchronizer")})
	hdf5_sink_node = inspiral_pipe.generic_node(hdf5_sink_job, dag, [], opts = hdf5_sink_options, output_files = {"rootdir": os.path.join(options.out_path, "gstlal_feature_hdf5_sink")})

#
# write out dag and sub files
#

dag.write_sub_files()
dag.write_dag()
dag.write_script()
